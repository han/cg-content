import numpy as np
def compute(uploaded_file_path):
    seq = {}
    with open(uploaded_file_path, 'r') as fin:
        for line in fin:
            if line.startswith('>'):
                name = line.replace('>', '').split()[0]
                seq[name] = ''
            else:
                seq[name] += line.replace('\n', '').strip()
        c_g_count = []
        name_all=[]
        for ID, value in seq.items():
            count = 0
            for i in value:
                if i=='C' or i=='G':
                    count+=1
            count = count/len(value) * 100
            c_g_count.append(count)
            name_all.append(ID)
        temp = np.array(c_g_count)
        idx = np.argmax(temp)
        end = f"ID: {name_all[idx]} with content: {round(c_g_count[idx],4)}"
    return end