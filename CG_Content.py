#!/usr/bin/python3
# Import Basic OS functions
import os
# Import modules for CGI handling
import cgi, cgitb, jinja2
import numpy as np
import calculating as cal
# enable debugging
import urllib.request

cgitb.enable()
# print content type
print("Content-type:text/html\r\n\r\n")

HTML = """
<html>
<head>
<title></title>
</head>
<body>

  <h1>CG-Content</h1>
  <h2> Please upload the sequence fasta-document and calculate the CG-Content </h2>
  <p style="font-size:48px">
    &#128512; &#128516;
  </p>
  <h3> The result include: </h3> 
  <p>
		<ul>
			<li>The ID of sequence with the highst CG-Content</li>
			<li>The CG-Content</li>
		</ul>
  </p>
  <form action="CG_Content.py" method="POST" enctype="multipart/form-data">
    File: <input name="file" type="file">
    <input name="submit" type="submit">
</form>

{% if filedata %}

{{filedata}}

{% endif %}

</body>
</html>
"""
inFileData = None

form = cgi.FieldStorage()  # get the data

UPLOAD_DIR = 'uploads'

if "file" in form:
    form_file = form['file']  # get the file name here

    if form_file.filename:

        uploaded_file_path = os.path.join(UPLOAD_DIR, os.path.basename(form_file.filename))
        # os.path.join() combine the path: upload directory and filename.

        with open(uploaded_file_path, 'wb') as fout:  # open the path and write the document in that path
                chunk = form_file.file.read()  # read(100000)
                #if not chunk:
                 #   break

                fout.write(chunk)

        # with open(uploaded_file_path, 'r') as fin:  # read the document
            # inFileData = ""
            # for line in fin:
            #     inFileData += line  # here pretent the line as a string
        inFileData = cal.compute(uploaded_file_path)

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0'}

print(jinja2.Environment().from_string(HTML).render(filedata=inFileData))



